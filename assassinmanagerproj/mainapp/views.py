from django.shortcuts import render, redirect, get_object_or_404
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseServerError, HttpResponseRedirect

from django.conf import settings

from django.core.exceptions import ObjectDoesNotExist

from django.utils.crypto import get_random_string
from django.db import transaction

from mainapp.forms import RegistrationForm, GameForm, ReportForm, EditProfileForm, ForgotPasswordForm, ResetPasswordForm, InviteForm, InviteAdminForm, SearchUserForm
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, authenticate
from mainapp.models import Profile, Game, Participant, Event
from django.db.models import Q

from django.core.mail import send_mail, EmailMultiAlternatives

from django.contrib import messages

import textwrap
import smtplib
import urllib

from  datetime import datetime, date

import copy, random


def checkIfConfirmedUser(request):
    return (request.user.is_active == True and Profile.objects.get(user=request.user).active == True)

def insertSearchBox(context):
    context["searchbox"] = SearchUserForm()
    
def requestIndex(request, context={}):
    insertSearchBox(context)
    renderPassedErrors(context)
    if (request.user.is_authenticated == True):
        context["logged_in"] = "You are currently logged in"
    else:
        context["logged_in"] = "You are not currently logged in"

    return render(request, 'mainapp/index.html', context)


def sendEmail(request, email, verificationCode, name, isNewUser):
    if isNewUser:
        subject = 'Assassin: The Game Confirmation Email'
    else:
        subject = 'Assassin: The Game Forgot Password Link Email'

    fromEmail = '15437team1@gmail.com'
    recipients = [email]
    if isNewUser:
        validationRelLink = "/confirm_acct/%s" % verificationCode
    else:
        validationRelLink = "/reset_password/%s" % verificationCode

    validationAbsLink = request.build_absolute_uri(validationRelLink)

    validationHtmlTag = "<a href='%s'>%s</a>" % (validationAbsLink, 
                                                 validationAbsLink)
    if isNewUser:
        text_content = textwrap.dedent("""
        Hello %s, 

        Thank you for registering for Assassin: The Game. 

        Visit the following link to validate your account:
        %s

        If the link doesnt work, copy and paste this link into your browser's address bar:
        %s

        Thanks!
        """) % (name, validationHtmlTag, validationAbsLink) 
    else:
        text_content = textwrap.dedent("""
        Hello %s, 

        Thank you for using Assassin: The Game. 

        Visit the following link to reset your account:
        %s

        If the link doesnt work, copy and paste this link into your browser's address bar:
        %s

        Thanks!
        """) % (name, validationHtmlTag, validationAbsLink) 

    html_content = text_content.replace("\n", "<br>")
    
    msg = EmailMultiAlternatives(subject, text_content, fromEmail, recipients)
    msg.attach_alternative(html_content, "text/html")
    msg.send()

def register(request):
    context = {}
    insertSearchBox(context)

    # Just display the registration form if this is a GET request
    if request.method == "GET":
        context["form"] = RegistrationForm()
        return render(request, 'mainapp/register.html', context)

    with transaction.atomic():
        form = RegistrationForm(request.POST, request.FILES)
        context['form'] = form
        if not form.is_valid():
            return render(request, 'mainapp/register.html', context)

        # Creates the new user from the valid form data
        new_user = User.objects.create_user(username=form.cleaned_data['username'],
                                            password=form.cleaned_data['password1'],
                                            first_name=form.cleaned_data['first_name'],
                                            last_name=form.cleaned_data['last_name'],
                                            email=form.cleaned_data['email'])
        new_user.save()

    #generate a random number
    code = get_random_string(length=20)

    profile = Profile(    user=new_user, 
                        verificationCode=code, 
                        active=False, 
                        image=request.FILES['prof_pic'])
    sendEmail(request, request.POST['email'], code, request.POST['first_name'], True)
    profile.save()

    new_user = authenticate(username=request.POST['username'], \
                            password=request.POST['password1'])
    login(request, new_user)


    # Logs in the new user and redirects to his/her todo list
    return requestIndex(request, context)


@transaction.atomic
@login_required
def confirm_acct(request, code):
    context = {}
    insertSearchBox(context)
    if (request.user.is_active == True and Profile.objects.get(user=request.user).active == True):
        context['errors'] = ['You already confirmed your account. Why are you confirming again?']
        return requestIndex(request, context)

    try:
        verified = Profile.objects.get(verificationCode=str(code), user=request.user)
        verified.active = True
        verified.save()
    except ObjectDoesNotExist:
        context['errors'] = ['The verification code is incorrect. Please follow the link through your email']
        return requestIndex(request, context)

    context['confirmed'] = 'Congrats! Your account is now confirmed! You can now start using the site.'
    return requestIndex(request, context)



@transaction.atomic
def forgotPassword(request):
    context = {}
    insertSearchBox(context)

    if (request.user.is_active == True and Profile.objects.get(user=request.user).active == True):
        context['errors'] = ['You have already logged in. Why are you trying to access forgot password page?']
        return requestIndex(request, context)
    if request.method == "GET":
        context["form"] = ForgotPasswordForm()
        return render(request, 'mainapp/forgot_password.html', context)

    form = ForgotPasswordForm(request.POST)
    context['form'] = form
    if not form.is_valid():
        return render(request, 'mainapp/forgot_password.html', context)
    try:
        userRequested = User.objects.get(username=form.cleaned_data['username'])
    except:
        context['errors'] = ['Alias does not exist. ']
        context["form"] = ForgotPasswordForm()
        return render(request, 'mainapp/forgot_password.html', context)


    #generate a random number
    code = get_random_string(length=20)
    requestedProfile = Profile.objects.get(user=userRequested)
    requestedProfile.passwordVerification = code
    sendEmail(request, userRequested.email, code,userRequested.first_name, False)
    requestedProfile.save()

    context["please_confirm"] = "Please follow the link through your email to reset your password"

    return requestIndex(request, context)

@transaction.atomic
def reset_password(request, code):
    context = {}
    insertSearchBox(context)

    if (request.user.is_active == True and Profile.objects.get(user=request.user).active == True):
        context['errors'] = ['You have already logged in. Why are you trying to reset your password?']
        return requestIndex(request, context)
    try:
        requestedProfile = Profile.objects.get(passwordVerification=code)
    except:
        context['errors'] = ['Invalid Reset Password Link']
        return requestIndex(request, context)

    if request.method == "GET":
        data = {"code": code}
        context["form"] = ResetPasswordForm(initial=data)
        return render(request, 'mainapp/reset_password.html', context)

    form = ResetPasswordForm(request.POST)
    context['form'] = form
    if not form.is_valid():
        return render(request, 'mainapp/reset_password.html', context)

    user = requestedProfile.user

    user.set_password(form.cleaned_data['password1'])


    user.save()
    requestedProfile.passwordVerification = ""
    requestedProfile.save()


    context["please_confirm"] = "Please log in with your new password"

    return requestIndex(request, context)
 
@login_required
def requestProfile(request, username):
    context = {}
    username = urllib.unquote(username)
    profileUser = User.objects.filter(username__iexact=username)[0]

    context["finished"] = False
    context['playing'] = False
    for game in profileUser.participantIn.all():
        if game.finished == True:
            context["finished"] =True
        else:
            context['playing'] = True

    context["profileUser"] = profileUser
    insertSearchBox(context)

    return render(request, "mainapp/profile.html", context)

def searchProfile(request):
    context = {}
    form = SearchUserForm(request.POST)
    context['searchbox'] = form
    if not form.is_valid():
        context["passed_errors"] = ["Invalid User!"]
        return requestIndex(request, context)
        # before = request.GET.get('from', None)
        # messages.success(request, "Invalid User!")
        # return HttpResponseRedirect(before)
    return requestProfile(request, form.cleaned_data['username'])

@login_required
@transaction.atomic
def editProfile(request):
    context = {}
    insertSearchBox(context)

    if request.method == "GET":
        defaultData = {
            "email": request.user.email,
            "first_name": request.user.first_name,
            "last_name": request.user.last_name
        }
        context["editForm"] = EditProfileForm(request.user, initial=defaultData)
        return render(request, "mainapp/editProfile.html", context)
    
    form = EditProfileForm(request.user, request.POST, request.FILES)

    if not form.is_valid():
        context["editForm"] = form
        messages.error(request, "Changes have not been saved, see errors for details.",
                       extra_tags="form-error")
        return render(request, "mainapp/editProfile.html", context)

    # update the user with the form's data
    formData = form.cleaned_data
    newPassword = formData.get("newPassword")
    if newPassword:
        request.user.set_password(newPassword)

    newProfPic = request.FILES.get("prof_pic")
    if newProfPic:
        request.user.profile.image = newProfPic

    request.user.email = formData["email"]
    request.user.first_name = formData["first_name"]
    request.user.last_name = formData["last_name"]

    request.user.save()
    request.user.profile.save()

    # when finished editing, tag with success message and 
    # redirect to edit profile page with new data
    messages.success(request, "Changes have been saved!", extra_tags="form-success")
    return redirect("mainapp:edit_profile")


def sendInviteEmail(request, gameSlug, invitee):

    subject = 'Assassin: The Game Invitation'


    fromEmail = '15437team1@gmail.com'
    recipients = [invitee.email]
    
    validationRelLink = "/join_game/%s" % gameSlug

    validationAbsLink = request.build_absolute_uri(validationRelLink)

    validationHtmlTag = "<a href='%s'>%s</a>" % (validationAbsLink, 
                                                 validationAbsLink)

    validationRelLink2 = "/leave_game/%s" % gameSlug

    validationAbsLink2 = request.build_absolute_uri(validationRelLink2)

    validationHtmlTag2 = "<a href='%s'>%s</a>" % (validationAbsLink2, 
                                                 validationAbsLink2)

    text_content = textwrap.dedent("""
        Hello %s, 

        %s has sent you a invite to join his game.. 

        Visit the following link to accept your invite:
        %s

        If the link doesnt work, copy and paste this link into your browser's address bar:
        %s

        Visit the following link to decline your invite:
        %s

        If the link doesnt work, copy and paste this link into your browser's address bar:
        %s

        Thanks!
        """) % (invitee.first_name,request.user.first_name, validationHtmlTag, validationAbsLink , validationHtmlTag2, validationAbsLink2) 


    html_content = text_content.replace("\n", "<br>")
    
    msg = EmailMultiAlternatives(subject, text_content, fromEmail, recipients)
    msg.attach_alternative(html_content, "text/html")
    msg.send()

@login_required
def deleteGame(request, gameSlug):
    context = {}
    insertSearchBox(context)

    try:
        requestedGame = Game.objects.get(slug=gameSlug)
    except:
        context["passed_errors"] = context.get("passed_errors", []) + ["Invalid Game"]
        return requestAllGames(request, context)

    if request.user not in requestedGame.admins.all():
        context["passed_errors"] = context.get("passed_errors", []) + ["You are not an admin of this game. You cannot delete the tournament. "]
        return  requestSpecificGame(request, gameSlug, context)

    if requestedGame.finished == True:
        context["passed_errors"] = context.get("passed_errors", []) + ["You cannot delete a game that has already finished. "]
        return  requestSpecificGame(request, gameSlug, context)


    try:
        events = Event.objects.filter(game=requestedGame)
        for e in events:
            e.delete()
    except:
        pass

    try:
        participants = Participant.objects.filter(game=requestedGame)
        for p in participants:
            p.delete()
    except:
        pass

    requestedGame.delete()

    return redirect("mainapp:all_games")



@login_required
def createGame(request):
    context = {}
    insertSearchBox(context)

    if (not checkIfConfirmedUser(request)):
        context["passed_errors"] = ["You are not logged in or your account is not confirmed yet. "]
        return requestAllGames(request, context) 

    if request.method == "GET":
        context["form"] = GameForm()
        return render(request, 'mainapp/createGame.html', context)

    with transaction.atomic():

        form = GameForm(request.POST)
        context['form'] = form
        if not form.is_valid():
            return render(request, 'mainapp/createGame.html', context)
            
        slug = Game.generateSlug(form.cleaned_data['name'])
        new_game = Game(name=form.cleaned_data['name'],
                        start_date=form.cleaned_data['start_date'],
                        auto_approve=form.cleaned_data['auto_approve'],
                        started= False,
                        finished=False,
                        winner=None,
                        active_players=0,
                        slug = slug)
        new_game.save()

    new_game.admins.add(request.user)
    new_game.save()

    people_to_invite = form.cleaned_data['participants_to_invite']
    if people_to_invite != "":
        for name in people_to_invite.split(';'):
            if (name.strip() == ""):
                continue
            user = User.objects.filter(username__iexact=(name.strip()))[0]
            if (user == request.user):
                new_game.participants.add(user)
            else:
                new_game.invited_participants.add(user)
                sendInviteEmail(request, slug, user)
                #TODO send a invite email
        new_game.save()

    people_to_invite = form.cleaned_data['admins_to_invite']
    if people_to_invite != "":
        for name in people_to_invite.split(';'):
            if (name.strip() == ""):
                continue
            user =User.objects.filter(username__iexact=(name.strip()))[0]
            new_game.admins.add(user)
                #TODO send a invite email
        new_game.save()

    return redirect("mainapp:specific_game", gameSlug=new_game.slug)

@login_required
def invitePeople(request, gameSlug, context = {}):
    insertSearchBox(context)
    renderPassedErrors(context)

    try:
        requestedGame = Game.objects.get(slug=gameSlug)
    except:
        context["passed_errors"] = context.get("passed_errors", []) + ["Invalid Game"]
        return requestAllGames(request, context)

    if request.user not in requestedGame.admins.all():
        context["passed_errors"] = context.get("passed_errors", []) + ["You are not an admin of this game. You cannot invite people. "]
        return  requestSpecificGame(request, gameSlug, context)

    if requestedGame.started == True:
        context["passed_errors"] = context.get("passed_errors", []) + ["Game has already started. You cannot invite people. "]
        return  requestSpecificGame(request, gameSlug, context)

    if request.method == "GET":
        data = {"slug": gameSlug}
        context["form"] = InviteForm(initial=data)
        return render(request, 'mainapp/invitePeople.html', context)

    form = InviteForm(request.POST)
    context['form'] = form
    if not form.is_valid():
        return render(request, 'mainapp/invitePeople.html', context)

    added = 0

    people_to_invite = form.cleaned_data['participants_to_invite']
    if people_to_invite != "":
        for name in people_to_invite.split(';'):
            if (name.strip() == ""):
                continue
            strippedname = name.strip()
            user = User.objects.filter(username__iexact=strippedname)[0]
            if (user == request.user):
                requestedGame.participants.add(user)
            else:
                requestedGame.invited_participants.add(user)
                sendInviteEmail(request, gameSlug, user)
                #TODO send a invite email
            added += 1

        requestedGame.save()

    if (added < 0):
        context["goodnews"] = context.get("goodnews", []) + ["People Invited. "]

    return requestSpecificGame(request, gameSlug, context)

@login_required
def inviteAdminPeople(request, gameSlug, context = {}):
    insertSearchBox(context)
    renderPassedErrors(context)

    try:
        requestedGame = Game.objects.get(slug=gameSlug)
    except:
        context["passed_errors"] = context.get("passed_errors", []) + ["Invalid Game"]
        return requestAllGames(request, context)

    if request.user not in requestedGame.admins.all():
        context["passed_errors"] = context.get("passed_errors", []) + ["You are not an admin of this game. You cannot add admins. "]
        return  requestSpecificGame(request, gameSlug, context)

    if requestedGame.finished == True:
        context["passed_errors"] = context.get("passed_errors", []) + ["Game has already finished. You cannot add admins. "]
        return  requestSpecificGame(request, gameSlug, context)

    if request.method == "GET":
        data = {"slug": gameSlug}
        context["form"] = InviteAdminForm(initial=data)
        return render(request, 'mainapp/inviteAdminPeople.html', context)

    form = InviteAdminForm(request.POST)
    context['form'] = form
    if not form.is_valid():
        return render(request, 'mainapp/inviteAdminPeople.html', context)

    added = 0
    people_to_invite = form.cleaned_data['admins_to_invite']
    if people_to_invite != "":
        for name in people_to_invite.split(';'):
            if (name.strip() == ""):
                continue
            strippedname = name.strip()
            user = User.objects.filter(username__iexact=strippedname)[0]
            requestedGame.admins.add(user)
            added += 1

        requestedGame.save()

    if added > 0:
        context["goodnews"] = context.get("goodnews", []) + ["Admins added. "]

    return requestSpecificGame(request, gameSlug, context)


def requestAllGames(request, context = {}):
    insertSearchBox(context)
    renderPassedErrors(context)
    allGames = Game.objects.all()
    context["games"] = allGames
    return render(request, 'mainapp/allGames.html', context)


def canUserReport(request, requestedGame, context):

    context["report"] = False
    try:
        requestedEvent = Event.objects.get(game=requestedGame, isActive=True, assassin=request.user, assassinReported=False)
        context["report"] = True
    except: 
        pass

    try:
        requestedEvent = Event.objects.get(game=requestedGame, isActive=True, target=request.user, targetReported=False)
        context["report"] = True
    except: 
        pass
    return

@transaction.atomic
def requestSpecificGame(request, gameSlug, context={}):
    insertSearchBox(context)
    renderPassedErrors(context)

    try:
        requestedGame = Game.objects.get(slug=gameSlug)
    except:
        context["passed_errors"] = context.get("passed_errors", []) + ["Invalid Game"]
        return requestAllGames(request, context)

    context["game"] = requestedGame
    context["is_admin"] = False
    context["can_join"] = False
    context["participant"] = False
    context["accept_invite"] = False
    context["requested_participant"] = False

    if request.user in requestedGame.admins.all():
        context["is_admin"] = True

    if (checkIfConfirmedUser(request) and 
        request.user not in requestedGame.participants.all() and
        request.user not in requestedGame.requested_participants.all() and
        request.user not in requestedGame.invited_participants.all()):
        context["can_join"] = True

    if (request.user in requestedGame.participants.all() or
        request.user in requestedGame.requested_participants.all()):
        context["participant"] = True

    if (request.user in requestedGame.requested_participants.all()):
        context["requested_participant"] = True

    if (request.user in requestedGame.invited_participants.all()):
        context["accept_invite"] = True

    context["view_target"] = False
    context["is_winner"] = False
    if requestedGame.started == True:
        try:
            requestedEvent = Event.objects.get(game=requestedGame, isActive=True, assassin=request.user)
            context["view_target"] = True
        except: 
            pass
        if requestedGame.finished == True:
            context['lastevent'] =  Participant.objects.filter(is_dead=True, game=requestedGame).order_by('-time_of_death')[:1][0]
            if requestedGame.winner == request.user:
                context["is_winner"] = True


    context["activeGameParticipants"] = Participant.objects.filter(game=requestedGame).order_by("person")
    canUserReport(request, requestedGame, context)


    return render(request, 'mainapp/game.html', context)

@login_required
@transaction.atomic
def joinGame(request, gameSlug):
    context = {}
    insertSearchBox(context)
    if (not checkIfConfirmedUser(request)):
        context["passed_errors"] = context.get("passed_errors", []) + ["You are not logged in or your account is not confirmed yet. "]
        return  requestSpecificGame(request, gameSlug, context)

    try:
        requestedGame = Game.objects.get(slug=gameSlug)
    except:
        context["passed_errors"] = context.get("passed_errors", []) + ["Invalid Game"]
        return requestAllGames(request, context)

    if requestedGame.started == True:
        context["passed_errors"] = context.get("passed_errors", []) + ["Game Started, You cannot join. "]
        return  requestSpecificGame(request, gameSlug, context)

    if request.user  in requestedGame.participants.all():
        context["passed_errors"] = context.get("passed_errors", []) + ["You are already a participant in the game. "]
        return requestSpecificGame(request, gameSlug, context)

    if request.user in requestedGame.requested_participants.all():
        context["passed_errors"] = context.get("passed_errors", []) + ["You already requested to be a participant in the game. "]
        return requestSpecificGame(request, gameSlug, context)

    if request.user in requestedGame.invited_participants.all():
        requestedGame.invited_participants.remove(request.user)
        requestedGame.participants.add(request.user)
        requestedGame.save()
    
        return redirect("mainapp:specific_game", gameSlug=gameSlug)

    if (requestedGame.auto_approve == True or request.user  in requestedGame.admins.all()):
        requestedGame.participants.add(request.user)
    else:
        requestedGame.requested_participants.add(request.user)

    requestedGame.save()
    
    return redirect("mainapp:specific_game", gameSlug=gameSlug)

@login_required
@transaction.atomic
def leaveGame(request, gameSlug):
    context = {}
    insertSearchBox(context)
    if (not checkIfConfirmedUser(request)):
        context["passed_errors"] = context.get("passed_errors", []) + ["You are not logged in or your account is not confirmed yet. "]
        return requestSpecificGame(request, gameSlug, context)

    try:
        requestedGame = Game.objects.get(slug=gameSlug)
    except:
        context["passed_errors"] = context.get("passed_errors", []) + ["Invalid Game"]
        return requestAllGames(request, context)

    #What if game has already started
    if requestedGame.started == True:
        context["passed_errors"] = context.get("passed_errors", []) + ["Game already started. You cannot leave the game "]
        return requestSpecificGame(request, gameSlug, context)

    if request.user not in requestedGame.participants.all() and request.user not in requestedGame.requested_participants.all() and request.user not in requestedGame.invited_participants.all():
        context["passed_errors"] = context.get("passed_errors", []) + ["You are not part of this game. "]
        return requestSpecificGame(request, gameSlug, context)

    if (request.user in requestedGame.participants.all()):
        requestedGame.participants.remove(request.user)
    elif (request.user in requestedGame.invited_participants.all()):
        requestedGame.invited_participants.remove(request.user)
    else:
        requestedGame.requested_participants.remove(request.user)

    requestedGame.save()
    
    return redirect("mainapp:specific_game", gameSlug=gameSlug)

@login_required
@transaction.atomic
def approve(request, gameSlug, userID, approve):
    context = {}
    insertSearchBox(context)

    try:
        requestedGame = Game.objects.get(slug=gameSlug)
    except:
        context["passed_errors"] = context.get("passed_errors", []) + ["Invalid Game"]
        return requestAllGames(request, context)

    try:
        requestedUser = User.objects.get(id=userID)
    except:
        context["passed_errors"] = context.get("passed_errors", []) + ["Invalid User"]
        return requestSpecificGame(request, gameSlug, context)

    if request.user not in requestedGame.admins.all():
        context["passed_errors"] = context.get("passed_errors", []) + ["You cannot approve or reject players as you are not an admin of the game."]
        return requestSpecificGame(request, gameSlug, context)

    if (requestedUser in requestedGame.requested_participants.all()):
        requestedGame.requested_participants.remove(requestedUser)
        if (int(approve) >=1):
            requestedGame.participants.add(requestedUser)
    elif ((requestedUser in requestedGame.invited_participants.all()) and (int(approve) == 0)):
        requestedGame.invited_participants.remove(requestedUser)
    else:
        context["passed_errors"] = context.get("passed_errors", []) + ["You cannot approve or reject players that are not requesting / invited to join."]
        return requestSpecificGame(request, gameSlug, context)

    return requestSpecificGame(request, gameSlug, context)

@login_required
@transaction.atomic
def removeAdmin(request, gameSlug, userID):
    context = {}
    insertSearchBox(context)

    try:
        requestedGame = Game.objects.get(slug=gameSlug)
    except:
        context["passed_errors"] = context.get("passed_errors", []) + ["Invalid Game"]
        return requestAllGames(request, context)

    try:
        requestedUser = User.objects.get(id=userID)
    except:
        context["passed_errors"] = context.get("passed_errors", []) + ["Invalid User"]
        return requestSpecificGame(request, gameSlug, context)


    if request.user not in requestedGame.admins.all():
        context["passed_errors"] = context.get("passed_errors", []) + ["You cannot remove an admin as you are not an admin of the game."]
        return requestSpecificGame(request, gameSlug, context)

    if request.user == requestedUser:
        context["passed_errors"] = context.get("passed_errors", []) + ["You cannot remove yourself as an admin"]
        return requestSpecificGame(request, gameSlug, context)

    if (requestedUser in requestedGame.admins.all()):
        requestedGame.admins.remove(requestedUser)
        context["goodnews"] = context.get("goodnews", []) + ["Admin %s removed." % (requestedUser.username)]
    else:
        context["passed_errors"] = context.get("passed_errors", []) + ["You cannot remove an person from the admin list when the person isnt an admin."]
        return requestSpecificGame(request, gameSlug, context)

    return requestSpecificGame(request, gameSlug, context)

@login_required
@transaction.atomic
def removePlayer(request, gameSlug, userID):
    context = {}
    insertSearchBox(context)

    try:
        requestedGame = Game.objects.get(slug=gameSlug)
    except:
        context["passed_errors"] = context.get("passed_errors", []) + ["Invalid Game"]
        return requestAllGames(request, context)

    try:
        requestedUser = User.objects.get(id=userID)
    except:
        context["passed_errors"] = context.get("passed_errors", []) + ["Invalid User"]
        return requestSpecificGame(request, gameSlug, context)


    if requestedGame.started == True:
        context["passed_errors"] = context.get("passed_errors", []) + ["Game Already Started. You cannot remove a player."]
        return requestSpecificGame(request, gameSlug, context)

    if request.user not in requestedGame.admins.all():
        context["passed_errors"] = context.get("passed_errors", []) + ["You cannot remove an player as you are not an admin of the game."]
        return requestSpecificGame(request, gameSlug, context)


    if (requestedUser in requestedGame.participants.all()):
        requestedGame.participants.remove(requestedUser)
        context["goodnews"] = context.get("goodnews", []) + ["Player %s removed." % (requestedUser.username)]
    else:
        context["passed_errors"] = context.get("passed_errors", []) + ["Player %s not part of tournament." % (requestedUser.username)]
        return requestSpecificGame(request, gameSlug, context)

    return requestSpecificGame(request, gameSlug, context)



@login_required
@transaction.atomic
def getTarget(request, gameSlug):
    context = {}
    insertSearchBox(context)
    try:
        requestedGame = Game.objects.get(slug=gameSlug)
    except:
        context["passed_errors"] = context.get("passed_errors", []) + ["Invalid Game"]
        return requestAllGames(request, context)

    try:
        requestedEvent = Event.objects.get(game=requestedGame, isActive=True, assassin=request.user)
    except:
        context["passed_errors"] = context.get("passed_errors", []) + ["You currently dont have an active opponent."]
        return requestSpecificGame(request, gameSlug, context)

    context['target'] = requestedEvent.target
    context["game"] = requestedGame

    try:
        context["profile"] = Profile.objects.get( user=requestedEvent.target)
    except ObjectDoesNotExist:
        # Should never happen
        return HttpResponseServerError()

    return render(request, 'mainapp/get_target.html', context)

def sendNewTargetEmail(assassin, target, game, request):
    subject = 'Assassin: The Game New Target '
    fromEmail = '15437team1@gmail.com'
    recipients = [assassin.email]
    validationRelLink = "/get_target/%s" % game.slug
    validationAbsLink = request.build_absolute_uri(validationRelLink)

    validationHtmlTag = "<a href='%s'>%s</a>" % (validationAbsLink, 
                                                 validationAbsLink)
    text_content = textwrap.dedent("""
    Hello %s, 

    We have assigned you a new target in the assassins game. 

    Visit the following link to view  your target:
    %s

    If the link doesnt work, copy and paste this link into your browser's address bar:
    %s

    Thanks!
    """) % (assassin.username, validationHtmlTag, validationAbsLink) 
    html_content = text_content.replace("\n", "<br>")
    
    msg = EmailMultiAlternatives(subject, text_content, fromEmail, recipients)
    msg.attach_alternative(html_content, "text/html")
    msg.send()


@login_required
def startGame(request, gameSlug):
    context = {}
    insertSearchBox(context)
    try:
        requestedGame = Game.objects.get(slug=gameSlug)
    except:
        context["passed_errors"] = context.get("passed_errors", []) + ["Invalid Game"]
        return requestAllGames(request, context)

    if request.user not in  requestedGame.admins.all() :
        context["passed_errors"] = context.get("passed_errors", []) + ["You cannot start the game as you are not an admin of the game. "]
        return requestSpecificGame(request, gameSlug, context)

    

    participants = requestedGame.participants.all()

    if participants.count() < 2:
        context["passed_errors"] = context.get("passed_errors", []) + ["You cannot start the game with less than 2 participants."]
        return  requestSpecificGame(request, gameSlug, context)

    with transaction.atomic():
        if requestedGame.started == True:
            context["passed_errors"] = context.get("passed_errors", []) + ["Game Already Started, You cannot start the game again. "]
            return  requestSpecificGame(request, gameSlug, context)

        requestedGame.started = True
        requestedGame.save()



    targets = list(copy.copy(participants))
    random.shuffle(targets)
    for i in xrange(len(targets)):
        j = (i+1) % (len(targets))
        chosen = targets[i]
        p = targets[j]
        next_id = requestedGame.getNextMatchId()
        event = Event(target=chosen, 
                      assassin=p, 
                      assigned=datetime.today(), 
                      death=datetime.today(), 
                      isActive=True, 
                      assassinReported=False,
                      targetReported=False,
                      game=requestedGame,
                      gameid=next_id)
        event.save()
        participant = Participant(person=p, game=requestedGame, is_dead=False, time_of_death=datetime.today(), current_target=event)
        participant.save()
        sendNewTargetEmail(p, chosen, requestedGame,request)

    requestedGame.invited_participants.clear()
    requestedGame.requested_participants.clear()

    
    requestedGame.start_date = datetime.today()
    requestedGame.active_players = participants.count()
    requestedGame.save()


    return requestSpecificGame(request, gameSlug)

@login_required
@transaction.atomic
def reportEvent(request, gameSlug):
    context = {}
    insertSearchBox(context)
    try:
        requestedGame = Game.objects.get(slug=gameSlug)
    except:
        context["passed_errors"] = context.get("passed_errors", []) + ["Invalid Game"]
        return requestSpecificGame(request, gameSlug, context)

    if (requestedGame.started == False):
        context["passed_errors"] = context.get("passed_errors", []) + ["Game has not started yet."]
        return  requestSpecificGame(request, gameSlug, context)

    if requestedGame.finished:
        context["passed_errors"] = context.get("passed_errors", []) + ["Game has ended. You can no longer report events."]
        return requestSpecificGame(request, gameSlug, context)

    try:
        participant = Participant.objects.get(person=request.user, game=requestedGame)         
    except:
        context["passed_errors"] = context.get("passed_errors", []) + ["You are not a participant in this game"]
        return requestSpecificGame(request, gameSlug, context)

    try:
        participant = Participant.objects.get(person=request.user, game=requestedGame, is_dead=False)         
    except:
        context["passed_errors"] = context.get("passed_errors", []) + ["You were already assassinated in this game"]
        return requestSpecificGame(request, gameSlug, context)

    if request.method == "GET":
        data = {"slug": gameSlug}
        context["form"] = ReportForm(initial=data)
        return render(request, 'mainapp/report.html', context)


    form = ReportForm(request.POST)
    context['form'] = form
    if not form.is_valid():
        return render(request, 'mainapp/report.html', context)

    if (form.cleaned_data['targeted'] == False):
        try:
            # Person is the targeted
            event = Event.objects.get(target=request.user, game=requestedGame, isActive=True)
            if event.targetReported == True:
                context["passed_errors"] = context.get("passed_errors", []) + ["You already reported your loss of life."]
                return requestSpecificGame(request, gameSlug, context)
            event.targetReported = True
            event.save()
        except:
            return HttpResponseServerError()

    else:
        try:
            event = Event.objects.get(assassin=request.user, game=requestedGame, isActive=True) 
            if event.assassinReported == True:
                context["passed_errors"] = context.get("passed_errors", []) + ["You already reported your kill."]
                return requestSpecificGame(request, gameSlug, context)   
            event.assassinReported = True
            event.save()     
        except:
            return HttpResponseServerError()

    if ((event.assassinReported == True and event.targetReported == False) or 
        (event.assassinReported == False and event.targetReported == True)):
        context["passed_errors"] = context.get("passed_errors", []) + ["The other person has to report the incident for this incident to be logged."]
        return requestSpecificGame(request, gameSlug, context)

    if (event.assassinReported == True and event.targetReported == True):
        event.isActive = False
        event.death = datetime.today()
        killer = event.assassin
        targeted = event.target
        killer_p = Participant.objects.get(person=killer, game=requestedGame, is_dead=False)  
        targeted_p = Participant.objects.get(person=targeted, game=requestedGame, is_dead=False) 
        targeted_p.time_of_death =datetime.today()
        targeted_p.is_dead = True
        target_of_targeted_event = targeted_p.current_target
        target_of_targeted_event.isActive = False
        event.save()
        targeted_p.save()
        target_of_targeted_event.save()
        requestedGame.active_players = requestedGame.active_players - 1;
        requestedGame.save()
        if (requestedGame.active_players == 1):
            requestedGame.finished = True
            requestedGame.winner = killer
            requestedGame.save() 
        else:
            next_id = requestedGame.getNextMatchId()
            event = Event(target=target_of_targeted_event.target, 
                          assassin=killer, 
                          assigned=datetime.today(), 
                          death=datetime.today(), 
                          isActive=True, 
                          assassinReported=False,
                          targetReported=False,
                          game=requestedGame,
                          gameid=next_id)
            event.save()
            killer_p.current_target = event
            killer_p.save()
            sendNewTargetEmail(killer, target_of_targeted_event.target, requestedGame,request)

        return requestSpecificGame(request, gameSlug, context)

    return HttpResponseServerError()

@login_required
def getHistoryForGame(request, gameSlug):
    context = {}
    insertSearchBox(context)
    try:
        requestedGame = Game.objects.get(slug=gameSlug)
    except:
        context["passed_errors"] = context.get("passed_errors", []) + ["Invalid Game"]
        return requestAllGames(request, context)

    if (requestedGame.started ==False):
        context["passed_errors"] = context.get("passed_errors", []) + ["Game has not started yet."]
        return  requestSpecificGame(request, gameSlug, context)

    context['game'] = requestedGame
    context['show_winner'] = True
    context['personal_history'] = False
    context['history'] = Event.objects.filter(game=requestedGame).order_by('gameid')


    return render(request, 'mainapp/historylog.html', context)

@login_required
@transaction.atomic
def getHistoryForGameUser(request, gameSlug, username):
    context = {}
    insertSearchBox(context)
    try:
        requestedGame = Game.objects.get(slug=gameSlug)
    except:
        context["passed_errors"] = context.get("passed_errors", []) + ["Invalid Game"]
        return requestAllGames(request, context)

    if (requestedGame.started ==False):
        context["passed_errors"] = context.get("passed_errors", []) + ["Game has not started yet."]
        return  requestSpecificGame(request, gameSlug, context)

    try:
        requestedUser = User.objects.get(username=username)
    except:
        context["passed_errors"] = context.get("passed_errors", []) + ["Invalid User"]
        return  requestSpecificGame(request, gameSlug, context)

    if requestedUser not in requestedGame.participants.all():
        ontext["errors"] = context.get("errors", []) + ["User not in tournament"]
        return  requestSpecificGame(request, gameSlug, context)



    context['game'] = requestedGame
    context['show_winner'] = False
    context['personal_history'] = True
    context['user'] = requestedUser


    context['history'] = Event.objects.filter(game=requestedGame).filter(Q(target=requestedUser) | (Q(assassin=requestedUser))).order_by('gameid')


    return render(request, 'mainapp/historylog.html', context)


def requestAbout(request):
    return HttpResponse("not yet implemented")


@transaction.atomic
def getLatest(request):
    context = dict()
    queryset = Event.objects.filter(isActive=False, assassinReported=True, targetReported=True)
    limit = 5
    count = queryset.count()
    if (count < limit):
        limit = count
    context['events'] = queryset.order_by('-death')[0:limit]


    return render(request, 'mainapp/recent.xml', context, content_type='application/xml');
def help(request):
    return render(request, 'mainapp/help.html', {})


def renderPassedErrors(context):
    context["errors"] = context.get("passed_errors", [])
    context["passed_errors"] = []



