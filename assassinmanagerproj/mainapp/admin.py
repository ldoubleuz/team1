from django.contrib import admin

from mainapp.models import Game, Profile

class GameAdmin(admin.ModelAdmin):
    list_display = ("name", "slug")
    readonly_fields = ("slug",)

    fieldsets = (
        (None, {'fields': ('name', 'slug', 'admins', 'participants')}),
    )

class ProfileAdmin(admin.ModelAdmin):
    pass

admin.site.register(Profile, ProfileAdmin)
admin.site.register(Game, GameAdmin)