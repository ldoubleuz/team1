(function(){
    var EVENT_LIST_CONTAINER_SELECTOR = "#recent-activity-container";
    var EVENT_LIST_SELECTOR = "#recent-activity-container .activity-list";
    var TIMER_STEP_MS = 15 * 1000; // 15 seconds

    function clearEventList(){
        $(EVENT_LIST_SELECTOR).empty()
    }

    function addListItem($titleContents, $contents, linkUrl){
        if (typeof($titleContents) === typeof("")){
            $titleContents = $("<span>").text($titleContents);
        }

        var $listItem = $("<div>").addClass("activity-item col-md-6");
        var $itemPanel = $("<div>").addClass("panel panel-default");
        $listItem.append($itemPanel);

        var $panelTitle = $("<div>").addClass("panel-heading");
        $itemPanel.append($panelTitle);

        var $panelTitleContents = $("<h4>").text("Game: ");
        if (linkUrl){
            $panelTitleContents.append($("<a>").attr("href", linkUrl).append($titleContents));
        } else {
            $panelTitleContents.append($("<span>").append($titleContents));
        }
        $panelTitle.append($panelTitleContents)

        var $panelBody = $("<div>").addClass("panel-body text-center").append($contents);
        $itemPanel.append($panelBody);

        // get the last row, if there is one
        var $eventList = $(EVENT_LIST_SELECTOR);
        var $rows = $(EVENT_LIST_SELECTOR).children(".row");
        var $lastRow
        if ($rows.length == 0) {
            // if no last row exists, create and append one
            $lastRow = $("<div>").addClass("row");
            $eventList.append($lastRow)
        } else {
            $lastRow = $($rows.get($rows.length-1));
        }

        // check if the row is full, and if so, create and add another row for
        // items
        if ($lastRow.children(".activity-item").length >= 2){
            $lastRow = $("<div>").addClass("row");
            $eventList.append($lastRow)
        }

        // finally, actually add the list item to its appropriate row
        $lastRow.append($listItem);
    }

    function onTimer(){
        $.ajax({
            url: "/get_latest",
            dataType: "xml",
            type: "GET",
            success: function(data){
                console.log("activity list refreshed!", data);
                clearEventList();

                var $users = $(data).find("user");
                if ($users.length == 0){
                    return
                }

                $users.each(function(i, userXml){
                    var $user = $(userXml);
                    var title = $user.find("gamename").text();
                    var slug = $user.find("slug").text();
                    var killerName = $user.find("killer").text();
                    var targetName = $user.find("target").text();
                    var killerImgSrc = $user.find("killerimgsrc").text();
                    var targetImgSrc = $user.find("targetimgsrc").text();
                    var time = $user.find("time").text();


                    var $leftImg = $("<img>").addClass("body-side img-thumbnail")
                                             .attr("src", killerImgSrc);
                    var $rightImg = $("<img>").addClass("body-side img-thumbnail dead-mugshot")
                                              .attr("src", targetImgSrc);

                    var $contentMain = $("<div>").addClass("body-main");
                    $contentMain.append(
                        $("<p>").text(killerName)
                    ).append(
                        $("<p>").text("eliminated").addClass("accent-text")
                    ).append(
                        $("<p>").text(targetName)
                    ).append(
                        $("<p>").append($("<small>").text(time))
                    );

                    var $itemContents = $("<div>").append($leftImg).append($contentMain).append($rightImg);

                    var url = "/games/"+slug+"/";
                    addListItem(title, $itemContents, url);
                });


                $(EVENT_LIST_CONTAINER_SELECTOR).fadeIn();
            }, 
            error: function(data){
                console.log("error", data);
                // dont change state of list
            },
            complete: function(){
                window.setTimeout(onTimer, TIMER_STEP_MS);
            }
        })
    }


    $(document).ready(function(){
       $(EVENT_LIST_CONTAINER_SELECTOR).hide();
       onTimer();
    });
})();