from django.db import models

from django.core.files.images import get_image_dimensions

from django.contrib.auth.models import User

from PIL import Image

def imgIsAnimated(img):
    try:
        img.seek(1)
        img.seek(0)
        return True
    except:
        return False

class Profile(models.Model):
    MAX_PROFPIC_WIDTH = 150
    MAX_PROFPIC_HEIGHT = 150

    user = models.OneToOneField(User)
    verificationCode = models.CharField(max_length=100)
    passwordVerification = models.CharField(max_length=100)
    active = models.BooleanField()
    image = models.ImageField(upload_to="profile_pictures", blank=True)

    gamesPlayed = models.IntegerField(default=0)
    numberOfTargetsKilled = models.IntegerField(default=0)

    def save(self):
        super(Profile, self).save()

        # resize the profile picture if exceeding size
        profPic = self.image
        if not profPic:
            return

        oldWidth, oldHeight = profPic.width, profPic.height
        maxWidth = Profile.MAX_PROFPIC_WIDTH
        maxHeight = Profile.MAX_PROFPIC_HEIGHT

        # process image to remove animation and thumbnail to fit maxsize
        img = Image.open(profPic.path)

        isAnimated = imgIsAnimated(img)
        doesNotFit = oldWidth > maxWidth or oldHeight > maxHeight

        # don't bother resaving image if no changes needed
        if not (isAnimated or doesNotFit):
            return

        # repaste image to remove animation
        if isAnimated:
            img.paste(img)

        # thumbnail image
        if oldWidth > maxWidth or oldHeight > maxHeight:
            widthScaleFactor = float(maxWidth) / oldWidth
            heightScaleFactor = float(maxHeight) / oldHeight
            # use minimum scale to ensure that the final dimensions fit in the container
            minScale = min(widthScaleFactor, heightScaleFactor)
            newWidth = int(min(oldWidth * minScale, maxWidth))
            newHeight = int(min(oldHeight * minScale, maxHeight))

            img.thumbnail((newWidth, newHeight), Image.ANTIALIAS)

        # update image (don't use optimize=True, this makes gifs transparent)
        img.save(profPic.path)


class Game(models.Model):
    MAX_SLUG_LEN = 50

    participants = models.ManyToManyField(User, blank=True, related_name="participantIn")
    admins = models.ManyToManyField(User, related_name="adminOf")
    requested_participants = models.ManyToManyField(User, blank=True, related_name="participantRequested")
    invited_participants = models.ManyToManyField(User, blank=True, related_name="invitedParticipants")


    active_players = models.IntegerField()
    name = models.CharField(max_length=100)
    auto_approve = models.BooleanField()
    winner = models.ForeignKey(User, related_name="winner", blank=True, null=True)

    start_date = models.DateTimeField()

    started = models.BooleanField()
    finished = models.BooleanField()

    last_match_id = models.IntegerField(default = 0)
    
    # to be used for displaying in the url
    slug = models.CharField(max_length=MAX_SLUG_LEN, unique=True)

    def getNextMatchId(self):
        self.last_match_id += 1
        self.save()
        next = self.last_match_id
        return next


    def save(self):
        if not self.slug:
            self.slug = Game.generateSlug(self.name)

        super(Game, self).save()

    @classmethod
    def generateSlug(cls, gameName):
        # get initial clean
        gameName = gameName.strip()
        origSlug = ""
        inSpaceGroup = False
        for i in xrange(len(gameName)):
            ch = gameName[i]
            if ch.isalnum():
                origSlug += ch
                inSpaceGroup = False
            elif ch.isspace():
                # ensure we don't double-add dash separators
                if not inSpaceGroup:
                    origSlug += "-"
                inSpaceGroup = True
            if len(origSlug) >= Game.MAX_SLUG_LEN:
                break
        origSlug = origSlug.strip("-")

        # get all games and make sure none with slug already exists
        allGames = Game.objects.all()
        # append numbers to the end of the slug if name conflicts exist
        finalSlug = origSlug
        tries = 1
        while True:
            conflict = allGames.filter(slug__iexact=finalSlug)
            if conflict:
                tries += 1
                suffix = "-%d" % tries
                keepLen = max(0, Game.MAX_SLUG_LEN - len(suffix))
                finalSlug = origSlug[:keepLen] + suffix
            else:
                break
        return finalSlug

# TODO: Akash, add some documentation to this! What do events represent, and
# when are they created/modified?
class Event(models.Model):
    target = models.ForeignKey(User, related_name="targetWho")
    assassin = models.ForeignKey(User, related_name="assassinWho")
    assigned = models.DateTimeField()
    death = models.DateTimeField()
    isActive = models.BooleanField()
    targetReported = models.BooleanField()
    assassinReported = models.BooleanField()
    game = models.ForeignKey(Game, related_name="gameIn")
    gameid = models.IntegerField()


class Participant(models.Model):
    person = models.ForeignKey(User, related_name="personWho")
    game = models.ForeignKey(Game, related_name="gameInP")
    is_dead = models.BooleanField(default=False)
    time_of_death = models.DateTimeField()
    current_target = models.ForeignKey(Event, related_name="currentEvent")
