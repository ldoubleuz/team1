from django import forms
from django.core.files.images import get_image_dimensions
from django.contrib.auth.models import User
from django.utils.safestring import *

from mainapp.models import Game, Profile

from datetime import datetime, date
from django.utils import timezone
import re



def getFieldMaxLength(modelClass, fieldName):
    field = modelClass._meta.get_field(fieldName, None)
    if field is None:
        return -1
    return field.max_length

class ForgotPasswordForm(forms.Form):
    username = forms.CharField(max_length=getFieldMaxLength(User, "username"),
                               label = mark_safe("What was your alias?:"),
                               widget = forms.TextInput(attrs = {"class":"form-control "}))

    def clean_username(self):
        username = self.cleaned_data.get("username")

        # only allow letters and numbers in username
        regex = re.compile(r"^[0-9a-zA-Z]*$")
        if not regex.match(username):
            raise forms.ValidationError("Username can only contain letters and numbers.")

        return username

class ReportForm(forms.Form):
    SUCCESS_KILL_TAG = "SUCCESS"
    FAILURE_KILL_TAG = "FAILURE"

    targeted = forms.ChoiceField(label="What happened?",
                                 choices=(
                                    (SUCCESS_KILL_TAG, 'I assassinated my target.'),
                                    (FAILURE_KILL_TAG, 'Someone assassinated me.'),
                                 ))
    slug = forms.CharField(max_length=200, widget=forms.HiddenInput())

    def clean_targeted(self):
        targeted = self.cleaned_data.get("targeted")
        return targeted == ReportForm.SUCCESS_KILL_TAG

class GameForm(forms.Form):
    name  = forms.CharField(max_length=100,
                            widget=forms.TextInput(attrs = {"class":"form-control"}))

    start_date = forms.DateTimeField(initial=datetime.today(),
                                 label="Estimated Start Date and Time of Game (yyyy-mm-dd hh:mm:ss)",
                                 widget = forms.DateTimeInput(attrs = {"class":"form-control"}))


    participants_to_invite = forms.CharField(label = "Enter aliases of participants to invite (semicolon separated)",
                                             widget = forms.TextInput(attrs = {"class":"form-control"}),
                                             required=False)

    admins_to_invite = forms.CharField(label = "Enter aliases of admin to have (semicolon separated)",
                                             widget = forms.TextInput(attrs = {"class":"form-control"}),
                                             required=False)

    auto_approve = forms.BooleanField(initial=True, label="Auto Approve Participants", required=False)

    def clean_name(self):
        name = self.cleaned_data.get("name")
        if Game.objects.filter(name__iexact=name):
            raise forms.ValidationError("Game Name Already Exists.")
        return name

    def clean_start_date(self):
        time = self.cleaned_data.get("start_date")
        if (timezone.make_aware(time, timezone.get_default_timezone()) < timezone.make_aware(datetime.today(), timezone.get_default_timezone())):
            raise forms.ValidationError("The Start date cannot be in the past.")
        return time    



    def clean_participants_to_invite(self):
        invitees = self.cleaned_data.get("participants_to_invite").strip()
        if (invitees == ''):
            return invitees

        users = ''
        regex = re.compile(r"^[0-9a-zA-Z]*$")
        for name in invitees.split(';'):
            strippedname = name.strip()
            if strippedname == "":
                continue
            if not regex.match(strippedname):
                raise forms.ValidationError("%s is not a valid username" % (strippedname))
            if not User.objects.filter(username__iexact=strippedname):
                raise forms.ValidationError("User %s does not exist" % (strippedname))

            actualuser = User.objects.filter(username__iexact=strippedname)[0]
            profile = Profile.objects.get(user=actualuser)
            if profile.active == False:
                raise forms.ValidationError("User %s is not confirmed" % (strippedname))                

            users += "%s;" % (strippedname)
        return invitees

    def clean_admins_to_invite(self):
        invitees = self.cleaned_data.get("admins_to_invite").strip()
        if (invitees == ''):
            return invitees

        users = ''
        regex = re.compile(r"^[0-9a-zA-Z]*$")
        for name in invitees.split(';'):
            strippedname = name.strip()
            if strippedname == "":
                continue
            if not regex.match(strippedname):
                raise forms.ValidationError("%s is not a valid username" % (strippedname))
            if not User.objects.filter(username__iexact=strippedname):
                raise forms.ValidationError("User %s does not exist" % (strippedname))

            actualuser = User.objects.filter(username__iexact=strippedname)[0]
            profile = Profile.objects.get(user=actualuser)
            if profile.active == False:
                raise forms.ValidationError("User %s is not confirmed" % (strippedname))       

            users += "%s;" % (strippedname)
        return invitees


class InviteForm(forms.Form):
    
    participants_to_invite = forms.CharField(label = "Enter aliases of participants to invite (semicolon separated)",
                                             widget = forms.TextInput(attrs = {"class":"form-control"}),
                                             required = True)
    slug = forms.CharField(max_length=200, widget=forms.HiddenInput())



    def clean(self):
        cleaned_data = super(InviteForm, self).clean()
        invitees = self.cleaned_data.get("participants_to_invite")

        if (invitees == None):
            return

        invitees = invitees.strip()

        if (invitees ==""):
            raise forms.ValidationError("You need to invite someone")

        regex = re.compile(r"^[0-9a-zA-Z]*$")

        if not Game.objects.filter(slug__iexact=self.cleaned_data.get("slug")):
            raise forms.ValidationError("Game Doesnt Exist.")

        requestedGame = Game.objects.get(slug=self.cleaned_data.get("slug"))


        for name in invitees.split(';'):
            strippedname = name.strip()
            if strippedname == "":
                continue
            if not regex.match(strippedname):
                raise forms.ValidationError("%s is not a valid username" % (strippedname))
            if not User.objects.filter(username__iexact=strippedname):
                raise forms.ValidationError("User %s does not exist" % (strippedname))
            user = User.objects.filter(username__iexact=strippedname)[0]
            if user in requestedGame.participants.all():
                raise forms.ValidationError("User %s already part of tournament" % (strippedname))
            if user in requestedGame.requested_participants.all():
                raise forms.ValidationError("User %s already requested to be part of this tournament" % (strippedname))
            if user in requestedGame.invited_participants.all():
                raise forms.ValidationError("User %s already invited to be part of this tournament" % (strippedname))
            profile = Profile.objects.get(user=user)
            if profile.active == False:
                raise forms.ValidationError("User %s is not confirmed" % (strippedname))  
        return cleaned_data

class InviteAdminForm(forms.Form):
    
    admins_to_invite = forms.CharField(label = "Enter aliases of admins to have (semicolon separated)",
                                             widget = forms.TextInput(attrs = {"class":"form-control"}),
                                             required = True)
    slug = forms.CharField(max_length=200, widget=forms.HiddenInput())



    def clean(self):
        cleaned_data = super(InviteAdminForm, self).clean()
        invitees = self.cleaned_data.get("admins_to_invite")

        if (invitees == None):
            return

        invitees = invitees.strip()

        if (invitees ==""):
            raise forms.ValidationError("You need to invite someone to be an admin")

        regex = re.compile(r"^[0-9a-zA-Z]*$")

        if not Game.objects.filter(slug__iexact=self.cleaned_data.get("slug")):
            raise forms.ValidationError("Game Doesnt Exist.")

        requestedGame = Game.objects.get(slug=self.cleaned_data.get("slug"))


        for name in invitees.split(';'):
            strippedname = name.strip()
            if strippedname == "":
                continue
            if not regex.match(strippedname):
                raise forms.ValidationError("%s is not a valid username" % (strippedname))
            if not User.objects.filter(username__iexact=strippedname):
                raise forms.ValidationError("User %s does not exist" % (strippedname))
            user = User.objects.filter(username__iexact=strippedname)[0]
            if user in requestedGame.admins.all():
                raise forms.ValidationError("User %s is already an admin" % (strippedname))
            profile = Profile.objects.get(user=user)
            if profile.active == False:
                raise forms.ValidationError("User %s is not confirmed" % (strippedname))  
        return cleaned_data


class EditProfileForm(forms.Form):
    def __init__(self, userToEdit, *args, **kwargs):
        self.userToEdit = userToEdit
        super(EditProfileForm, self).__init__(*args, **kwargs)

    origPassword = forms.CharField(max_length=100,
                                   label="Current password (required to make changes)",
                                   widget=forms.PasswordInput(attrs={"class":"form-control"}))

    email = forms.CharField(max_length=getFieldMaxLength(User, "email"),
                            widget=forms.EmailInput(attrs={"class":"form-control"}))

    first_name = forms.CharField(max_length=getFieldMaxLength(User, "first_name"),
                                 widget=forms.TextInput(attrs={"class":"form-control"}))

    last_name = forms.CharField(max_length=getFieldMaxLength(User, "last_name"),
                                widget=forms.TextInput(attrs={"class":"form-control"}))

    prof_pic = forms.ImageField(label="New profile picture (Headshot Preferred) (optional)" 
                                       ,
                                widget=forms.ClearableFileInput(attrs={"accept": "image/*"}),
                                required=False)

    newPassword = forms.CharField(max_length=100,
                                   label="New password (optional)",
                                   widget=forms.PasswordInput(attrs={"class":"form-control"}),
                                   required=False)
    confirmNewPassword = forms.CharField(max_length=100,
                                   label="Retype new password",
                                   widget=forms.PasswordInput(attrs={"class":"form-control"}),
                                   required=False)

    def clean_origPassword(self):
        origPassword = self.cleaned_data.get("origPassword")
        if not self.userToEdit.check_password(origPassword):
            raise forms.ValidationError("Incorrect password given.")
        return origPassword



    def clean_confirmNewPassword(self):
        newPassword1 = self.cleaned_data.get("newPassword")
        newPassword2 = self.cleaned_data.get("confirmNewPassword")

        if (newPassword1 or newPassword2) and newPassword1 != newPassword2:
            raise forms.ValidationError("New passwords must match.")
        return newPassword1

class ResetPasswordForm(forms.Form):


    password1 = forms.CharField(max_length=100, 
                                 label="New Password",
                                 widget=forms.PasswordInput(attrs = {"class":"form-control"}))

    password2 = forms.CharField(max_length=100,
                                 label="Confirm New password",
                                 widget=forms.PasswordInput(attrs = {"class":"form-control"}))
    code = forms.CharField(max_length=100, widget=forms.HiddenInput())



    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords must match.")
        return password2

  


class RegistrationForm(forms.Form):
    username = forms.CharField(max_length=getFieldMaxLength(User, "username"),
                               label = mark_safe("Alias (displayed name):"),
                               widget = forms.TextInput(attrs = {"class":"form-control "}))

    email = forms.EmailField(max_length=getFieldMaxLength(User, "email"),
                             widget = forms.EmailInput(attrs = {"class":"form-control"}))

    password1 = forms.CharField(max_length=100, 
                                 label="Password",
                                 widget=forms.PasswordInput(attrs = {"class":"form-control"}))

    password2 = forms.CharField(max_length=100,
                                 label="Confirm password",
                                 widget=forms.PasswordInput(attrs = {"class":"form-control"}))

    first_name = forms.CharField(max_length=getFieldMaxLength(User, "first_name"),
                                 widget = forms.TextInput(attrs = {"class":"form-control"}))

    last_name = forms.CharField(max_length=getFieldMaxLength(User, "last_name"),
                                widget = forms.TextInput(attrs = {"class":"form-control"}))

    prof_pic = forms.ImageField(label="Profile picture (Headshot Preferred)" 
                                       ,
                                widget=forms.ClearableFileInput(attrs={"accept": "image/*"}))

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords must match.")
        return password2

    def clean_username(self):
        username = self.cleaned_data.get("username")

        # only allow letters and numbers in username
        regex = re.compile(r"^[0-9a-zA-Z]*$")
        if not regex.match(username):
            raise forms.ValidationError("Username can only contain letters and numbers.")

        if User.objects.filter(username__iexact=username):
            raise forms.ValidationError("Account already exists.")
        return username

    def clean_first_name(self):
        firstName = self.cleaned_data.get("first_name")
        return firstName.strip().title()

    def clean_last_name(self):
        lastName = self.cleaned_data.get("last_name")
        return lastName.strip().title()

class SearchUserForm(forms.Form):
    username = forms.CharField(max_length=getFieldMaxLength(User, "username"),
                               label = mark_safe("Alias Search:"),
                               widget = forms.TextInput(attrs = {"class":"form-control "}))


    def clean_username(self):
        username = self.cleaned_data.get("username")

        # only allow letters and numbers in username
        regex = re.compile(r"^[0-9a-zA-Z]*$")
        if not regex.match(username):
            raise forms.ValidationError("Username can only contain letters and numbers.")

        if not User.objects.filter(username__iexact=username):
            raise forms.ValidationError("Account doesn't exist.")

        return username
