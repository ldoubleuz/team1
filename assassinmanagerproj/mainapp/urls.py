from django.conf.urls import patterns, include, url

urlpatterns = patterns("",
    url(r"^$", 'mainapp.views.requestIndex', name="index"),
    url(r"^profile/(?P<username>[0-9a-zA-Z]+)/$", 'mainapp.views.requestProfile', name="view_profile"),
    url(r"^edit_profile/$", 'mainapp.views.editProfile', name="edit_profile"),
    url(r"^search_profile/$", 'mainapp.views.searchProfile', name="search_profile"),
    url(r"^games/$", 'mainapp.views.requestAllGames', name="all_games"),
    # url for requesting info on a specific game
    url(r"^games/(?P<gameSlug>[\w-]+)/$", 'mainapp.views.requestSpecificGame', name="specific_game"),
    url(r"^games/(?P<gameSlug>[\w-]+)/history/$", 'mainapp.views.getHistoryForGame', name="game_history"),
    url(r"^games/(?P<gameSlug>[\w-]+)/user_history/(?P<username>[0-9a-zA-Z]+)/$", 'mainapp.views.getHistoryForGameUser', name="user_game_history"),
    url(r"^get_latest/$", 'mainapp.views.getLatest', name="get_latest"),
    url(r"^join_game/(?P<gameSlug>[\w-]+)/$", 'mainapp.views.joinGame', name="join_game"),
    url(r"^invite_people/(?P<gameSlug>[\w-]+)/$", 'mainapp.views.invitePeople', name="invite_people"),
    url(r"^invite_admin_people/(?P<gameSlug>[\w-]+)/$", 'mainapp.views.inviteAdminPeople', name="invite_admin_people"),
    url(r"^leave_game/(?P<gameSlug>[\w-]+)/$", 'mainapp.views.leaveGame', name="leave_game"),
    url(r"^approve/(?P<gameSlug>[\w-]+)/(?P<userID>\d+)/(?P<approve>\d+)/$", 'mainapp.views.approve', name="approve"),
    url(r"^remove_admin/(?P<gameSlug>[\w-]+)/(?P<userID>\d+)/$", 'mainapp.views.removeAdmin', name="remove_admin"),
    url(r"^remove_player/(?P<gameSlug>[\w-]+)/(?P<userID>\d+)/$", 'mainapp.views.removePlayer', name="remove_player"),    
    url(r"^startGame/(?P<gameSlug>[\w-]+)/$", 'mainapp.views.startGame', name="start_game"),
    url(r"^about/$", 'mainapp.views.requestAbout', name="about"),
    url(r"^deleteGame/(?P<gameSlug>[\w-]+)/$", 'mainapp.views.deleteGame', name="delete_game"),
    url(r"^get_target/(?P<gameSlug>[\w-]+)/$", 'mainapp.views.getTarget', name="get_target"),
    url(r"^report_event/(?P<gameSlug>[\w-]+)/$", 'mainapp.views.reportEvent', name="report_event"),

    url(r"^createGame/$", "mainapp.views.createGame", name="create_game"),

    url(r'^register/$', 'mainapp.views.register', name='register'),
    url(r'^confirm_acct/(?P<code>\w+)/$', 'mainapp.views.confirm_acct', name='confirm_acct'),
    url(r'^reset_password/(?P<code>\w+)/$', 'mainapp.views.reset_password', name='reset_password'),
    url(r'^forgotPassword/$', 'mainapp.views.forgotPassword', name='forgot_password'),
    url(r'^login/$', 'django.contrib.auth.views.login', {'template_name':'mainapp/login.html'}, name='login'),
    url(r'^help/$', 'mainapp.views.help', name='help'),
    # Route to logout a user and send them back to the login page
    url(r'^logout/$', 'django.contrib.auth.views.logout', 
        {"next_page": "mainapp:index"}, name="logout"),
)